from django import forms
from django import template

register = template.Library()

@register.filter(name="make_PasswordInput")	
def make_PasswordInput(field):
    return field.as_widget(widget=forms.PasswordInput())
	