from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError
from .forms import LoginForm, SignUpForm
from .models import User


def log_user_in(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if not form.is_valid():
            return render(request, 'my_account/login.html', {'form':form})
        else:
            email = request.POST['email']
            password = request.POST['password']
			
            user = authenticate(email=email, password=password)
            if user is None:
                context = {
                    'form':form,
                    'invalid_credentials':True
                }
                return render(request,'my_account/login.html',context)
				
            else:
                login(request, user)				
                return redirect('/forecast/')
    else:
        form = LoginForm()		
        return render(request, 'my_account/login.html', {'form':form})

def log_user_out(request):	
    logout(request) 
    return redirect('/')		
	
def sign_up(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if not form.is_valid():
            return render(request, 'my_account/sign_up.html', {'form':form})
        else:
            try:
                o = User(
                    email    = form.cleaned_data['email'],
                    password = form.cleaned_data['password']	
                )
                o.save()
                return redirect('/my-account/success/')
            except:
                context = {
                    'form':form,
                    'account_taken':True					
                }			
                return render(request, 'my_account/sign_up.html', context)
    else:
        form = SignUpForm()		
        return render(request, 'my_account/sign_up.html', {'form':form})

def success(request):
    query_set = User.objects.all()
    
    return render(request, 'my_account/success.html')