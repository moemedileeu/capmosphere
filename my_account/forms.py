from django import forms

class AbstractEmailPasswordForm(forms.Form):
    email = forms.EmailField(strip = True, error_messages = {"required":"what's your email?", "invalid":"this email is not legit"})
    password = forms.CharField(strip = True, error_messages = {"required":"what's your password?"})

#--------------------------------------------
#subclasses of the AbstractEmailPasswordForm
#--------------------------------------------
class LoginForm(AbstractEmailPasswordForm):
    ''' '''
	
class SignUpForm(AbstractEmailPasswordForm):
    password_again = forms.CharField(strip = True, error_messages = {"required":"please confirm your password"})
	
    def clean(self):
        cleaned_data = super(SignUpForm, self).clean()
        password = cleaned_data.get("password")
        password_again = cleaned_data.get("password_again")

        if password != password_again:
            msg = "your passwords did not match "
            self.add_error('password', msg)            
            self.add_error('password_again', msg)
