from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$|^login/$', views.log_user_in),
	url(r'^$|^logout/$', views.log_user_out),
	url(r'^sign-up/$', views.sign_up),
	url(r'^success/$', views.success)
]
