from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager 

class UserManager(BaseUserManager):
    def _create_user(self, email, password, **kwargs):
        if not email:
            raise ValueError('this email address is not legit')
			
        if kwargs['is_superuser']:
            email = email
        else:
            email = self.normalize_email(email)
			
        user = self.model(
            email = email,
            **kwargs
        )
            
        user.set_password(password)       
        user.save(using=self._db)
        return user

    def create_user(self, email, password, **kwargs):
        kwargs.setdefault('is_superuser', False)
        self._create_user( email, password, **kwargs)
		
    def create_superuser(self, email, password, **kwargs):
        kwargs.setdefault('is_superuser', True)
        kwargs.setdefault('is_admin', True)
        kwargs.setdefault('is_staff', True)
        self._create_user( email, password, **kwargs)

class User(AbstractBaseUser):
    email        = models.EmailField(unique=True)
    is_active    = models.BooleanField(default=True)
    is_admin     = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    date_joined  = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'
	
    objects = UserManager()
	
    def get_full_name(self):
        return self.email
		
    def get_short_name(self):
        return self.email
		
    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True
		
    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.is_admin