from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.template import RequestContext 
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import WeatherRecord

@login_required
def heads_up(request):
    request_context = RequestContext(request)
    return render(request, 'forecast/heads_up.html', request_context)
	
@login_required
def weather_records(request):
    weather_records = WeatherRecord.objects.all()
    paginator = Paginator(weather_records, 3)

    page = request.GET.get('page')
    try:
        weather_records = paginator.page(page)
    except PageNotAnInteger:
        weather_records = paginator.page(1)
    except EmptyPage:
        weather_records = paginator.page(paginator.num_pages)
		
    return render(request, 'forecast/weather_records.html', {'weather_records':weather_records})
	

