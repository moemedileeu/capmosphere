from django.db import models

class WeatherRecord(models.Model):
    date     = models.CharField(max_length=30)
    min_temp = models.CharField(max_length=30)
    max_temp = models.CharField(max_length=30)
    wind     = models.CharField(max_length=30)
    rain     = models.CharField(max_length=30)
