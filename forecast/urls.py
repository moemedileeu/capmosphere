from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.heads_up),
	url(r'^weather-records/$', views.weather_records),
]
