from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.index),
	url(r'^suggestion-box/$', views.suggestion_box),	
]
